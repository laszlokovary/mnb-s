package com.lkovari.mobile.apps.mnb;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiManager;

/**
 * 
 * @author lkovari
 *
 */
public class ConnectionManager {


	/**
	 * 
	 * @param activity - Activity of application
	 * @return true if WiFi is connecting or connected
	 */
	public static boolean isWiFiConnected(Activity activity) {
		ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		State state = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
		return ((state == State.CONNECTED) || (state == State.CONNECTING));
	}

	/**
	 * 
	 * @param activity - Activity of application
	 * @return true if Mobile is connecting or connected
	 */
	public static boolean isMobileConnected(Activity activity) {
		ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		State state = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
		return ((state == State.CONNECTED) || (state == State.CONNECTING));
	}
	

	/**
	 * 
	 * @param activity
	 * @return
	 */
	public static boolean isWiFiEnabled(Activity activity) {
		WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
		return wifiManager.isWifiEnabled();
	}	
	
	/**
	 * 
	 * @param activity
	 */
	public static void turnOnWiFi(Activity activity) {
		WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
		if(wifiManager.isWifiEnabled()){
			// nothing to do
		}else{
		    wifiManager.setWifiEnabled(true);
		}		
	}
	
	/**
	 * 
	 * @param activity
	 */
	public static void turnOffWiFi(Activity activity) {
		WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
		if(wifiManager.isWifiEnabled()){
		    wifiManager.setWifiEnabled(false);
		}else{
			// nothing to do
		}		
	}
	
}
