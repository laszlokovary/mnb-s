package com.lkovari.mobile.apps.mnb;

/**
 * 
 * @author lkovari
 *
 */
public class ExchangeRate {
	private String currency;
	private double rate;
	private int unit;
	
	public ExchangeRate(String curr, String rate, int unit) {
		this.currency = curr;
		if (rate.contains(",")) {
			String[] rated = rate.split(",");
			rate = rated[0] + "." + rated[1];
			this.rate = Double.parseDouble(rate);
		}
		else {
			this.rate = Double.parseDouble(rate);
		}
		this.unit = unit;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public double getRate() {
		return rate;
	}
	
	public int getUnit() {
		return unit;
	}
}
