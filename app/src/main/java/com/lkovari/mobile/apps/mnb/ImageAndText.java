package com.lkovari.mobile.apps.mnb;

/**
 * 
 * @author lkovari
 *
 */
public class ImageAndText {
    private int imageId;
    private String currencyName;
    private double currencyRate;
 
    public ImageAndText(int imageId, String currencyName, double rate) {
        this.imageId = imageId;
        this.currencyName = currencyName;
        this.currencyRate = rate;
    }

    public int getImageId() {
        return imageId;
    }

    /**
     *
     * @return String - 3 char currency name
     */
    public String getCurrencyName() {
		return currencyName;
	}

    /**
     *
     * @return double - the currency rate
     */
    public double getCurrencyRate() {
		return currencyRate;
	}
}
