package com.lkovari.mobile.apps.mnb;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author lkovari
 *
 */
public class Info extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);

        ImageView imageView = (ImageView) findViewById(R.id.logoImageView);
        imageView.setImageResource(R.drawable.icon);

        TextView appnameTextView = (TextView) findViewById(R.id.appnameTextView);
        appnameTextView.setText(R.string.about_appname);

        TextView descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
        descriptionTextView.setText(R.string.about_description);

        TextView authorTextView = (TextView) findViewById(R.id.authorTextView);
        authorTextView.setText(R.string.about_author);
        
    }    
}
