package com.lkovari.mobile.apps.mnb;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * http://www.androidpeople.com/android-xml-parsing-tutorial-%E2%80%93-using-domparser
 * @author lkovari
 *
 */
public class MNVWebServiceManager {
	private static final String SOAP_ACTION = "http://www.mnb.hu/webservices/GetCurrentExchangeRates";
	private static final String NAMESPACE = "http://www.mnb.hu";
	private static final String URL = "http://www.mnb.hu/arfolyamok.asmx?WSDL";

    /**
     *
     * @return String - response of webservice
     * @throws IOException
     * @throws XmlPullParserException
     */
    public static String requestCurrencies() throws IOException, XmlPullParserException {
        String operationRequest = "GetCurrenciesRequest";
        return executeSOAPRequest(operationRequest);
    }

    /**
     *
     * @return String
     * @throws IOException
     * @throws XmlPullParserException
     */
    public static String requestCurrencyUnits() throws IOException, XmlPullParserException {
        String operationRequest = "GetCurrencyUnits";
        return executeSOAPRequest(operationRequest);
    }

    /**
     *
     * @return String
     * @throws IOException
     * @throws XmlPullParserException
     */
    public static String requestCurrentExchangeRates() throws IOException, XmlPullParserException {
        String operationRequest = "GetCurrentExchangeRates";
        return executeSOAPRequest(operationRequest);
    }

    /**
     *
     * @return String
     * @throws IOException
     * @throws XmlPullParserException
     */
    public static String requestDateInterval() throws IOException, XmlPullParserException {
        String operationRequest = "GetDateIntervalRequest";
        return executeSOAPRequest(operationRequest);
    }

    /**
     *
     * @return String
     * @throws IOException
     * @throws XmlPullParserException
     */
    public static String requestExchangeRates() throws IOException, XmlPullParserException {
        String operationRequest = "GetExchangeRatesRequest";
        return executeSOAPRequest(operationRequest);
    }

    /**
     *
     * @return String
     * @throws IOException
     * @throws XmlPullParserException
     */
    public static String requestInfo() throws IOException, XmlPullParserException {
        String operationRequest = "GetInfoRequest";
        return executeSOAPRequest(operationRequest);
    }

	public static String executeSOAPRequest(String operation) throws IOException, XmlPullParserException {
        // set up request
		SoapPrimitive request = new SoapPrimitive(NAMESPACE, operation, "");
        // put all required data into a soap envelope
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
        // prepare request
		envelope.setOutputSoapObject(request);
		HttpTransportSE httpTransport = new HttpTransportSE(URL);
        // this is optional, use it if you don't want to use a packet sniffer to check what the sent message was (httpTransport.requestDump)
		httpTransport.debug = true;
        // send request
		httpTransport.call(SOAP_ACTION, envelope);
        // get response
		SoapPrimitive result = (SoapPrimitive)envelope.getResponse();
		return result.toString();
	}
}
